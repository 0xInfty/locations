const service = require('./servicio')
const {logger} = require('../../logger')

async function create(req, res){
    try{
        const location = req.body
        logger.info({ action: 'Request to create location' })
        entryId = await service.createLocation(location)
        logger.info({ action: 'Location created' })
        res.writeHead(201, 'Content-Type', 'application/json')
        res.end(JSON.stringify(entryId))
    } catch {
        logger.info({ action: 'Fail en location creation' })
        res.writeHead(500, 'Content-Type', 'application/json')
        res.end(JSON.stringify({ result:"Fail en location creation"}))
    }
}

async function getAll(req, res){

    logger.info({ action: 'request to get locations'})

    const locations = await service.getLocations()
    console.log("controller: " + locations)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(locations))
}

async function getTypes(req, res){

    logger.info({ action: 'request to get location types'})

    const locationTypeObjects = await service.getLocationTypes()
    var locationTypes = []
    for (var piece of locationTypeObjects) {
      locationTypes.push( piece['name'] )
    }
    console.log("controller: " + locationTypes)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(locationTypes))
}

async function search(req, res){

    logger.debug({ action: 'request to search location'})
    const searchString = req.body.searchString

    const objectsFound = await service.search(searchString)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(objectsFound))
}

async function searchRecent(req, res){

    logger.debug({ action: 'request to search recent locations'})
    const maxNumber = req.body.maxNumber

    const objectsFound = await service.searchRecent(maxNumber)
    
    res.writeHead(200, 'Content-Type', 'application/json')
    res.end(JSON.stringify(objectsFound))
}

module.exports = { create, getAll, getTypes, search, searchRecent }