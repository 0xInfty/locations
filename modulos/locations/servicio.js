const { ObjectID } = require("bson");
const { getDb } = require("../../mongodb");

const verbosityLevel = 1;
const consoleLogDebug = (stringToConsoleLog, importanceLevel=0) => {
    if ( importanceLevel <= verbosityLevel ) { console.log( stringToConsoleLog ) }
}

async function createLocationType( locationType ) { return new Promise(async (resolve, reject) => { 

    const locationsTypesDocuments = await getDb().collection('location_types').findOne({name: locationType})

    if(!locationsTypesDocuments) {
        getDb().collection('location_types').insertOne({name: locationType}).then( () => {   
            consoleLogDebug(`Location Type successfully added: ${locationType}`, 1)
            resolve ({'resultLocationType':'success' })
        }).catch( error => {
            consoleLogDebug(`Location Type not successfully added: ${locationType}`)
            console.error(error)
            reject(error)
        })   
    } else{
        resolve ({'resultLocationType':'success' })
    }
})}

async function createLocation(location){ return new Promise(async (resolve, reject) => {
    
    location.created_ts = Date.now()
    const entryId = ObjectID()
    location._id = entryId
    const entryIdString = ObjectID(entryId).toString()
    consoleLogDebug(entryId, 1)

    await getDb().collection('locations').insertOne(location).catch( error => {
        console.error(error);
        reject(error);
    })

    const locationType = location.locationType;
    await createLocationType(locationType).catch( error => {
        console.error(error);
        reject(error);
    })

    consoleLogDebug(entryIdString, 1)
    resolve( entryIdString )
})}

async function getLocations(){ return new Promise((resolve, reject) => {
    consoleLogDebug("request to get all locations", 1);
    getDb().collection('locations').find().toArray(function (err, docs) {
        for (const doc of docs){ doc.entryClass = "locations" }
        consoleLogDebug(docs, 2)
        resolve (docs);
    })
})}

async function getLocationTypes(){ return new Promise((resolve, reject) => {
    consoleLogDebug("request to get all location types", 1);
    getDb().collection('location_types').find().toArray(function (err, docs) {
        consoleLogDebug(docs, 2)
        resolve (docs);
    })
})}

async function search( searchString ){ return new Promise((resolve, reject) => {
    consoleLogDebug("request to search locations, string: " + searchString, 1);
    getDb().collection('locations').find({ $text: {$search: searchString}},{ score:{ $meta: "textScore"}}).toArray(function (err, docs) {
        for (const doc of docs){ doc.entryClass = "locations" }
        consoleLogDebug(docs, 2)
        resolve (docs);
    })
})}

async function searchRecent( maxNumber ) { return new Promise((resolve, reject) => {
    consoleLogDebug(`request to search the most recent ${maxNumber} locations`, 1);
    getDb().collection('locations').find().sort({"created_ts": -1}).limit(maxNumber).toArray(function (err, docs) {
        for (const doc of docs){ doc.entryClass = "locations" }
        consoleLogDebug(docs, 2)
        resolve (docs);
    })
})}

module.exports = { createLocation, getLocations, getLocationTypes, search, searchRecent }