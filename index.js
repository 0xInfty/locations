const express = require('express');
const locationsController = require('./modulos/locations/controlador');
const { connectToServer } = require("./mongodb");
const {logger} = require('./logger')


const port = process.env.LOCATIONS_PORT

async function runServer(){
  process.on('uncaughtException', err => {
    logger.error({action: 'Fatal error', data:{error:err}});
    setTimeout(() => { process.exit(0) }, 1000).unref() 
  })

  process.on('unhandledRejection', (err, err2) => {
    logger.error({action: 'Fatal error', data:{error:err}});
    setTimeout(() => { process.exit(0) }, 1000).unref() 
  })

  process.on("SIGTERM", (err) => {
    logger.error({action: "Stopping...", data: {error: err}});
    setTimeout(() => { mprocess.exit(1);}, 100).unref();
  });

  try {
    connectToServer();
    logger.info({ event:"MongoDB loaded and connected" });
  } catch (error) {
    logger.error({ event: "MongoDB ERROR: ", error });
    process.exit(1);
  }

  const app = express()

  // Parser de body y cookies
  app.use(express.json());
  app.use(express.urlencoded({ extended: true }));

  app.get("/test", (req, res) => {
    res.header("Content-Type", "application/json");
    res.writeHead(200);
    res.end("Todo ok");
  });

  app.post('/locations/create', (req, res) => { 
    locationsController.create(req, res);
  })

  app.post('/locations/locations', (req, res) => { 
    locationsController.getAll(req, res);
  })

  app.post('/locations/location-types', (req, res) => { 
    locationsController.getTypes(req, res);
  })

  app.post('/locations/search', (req, res) => { 
    locationsController.search(req, res);
  })

  app.post('/locations/recent', (req, res) => { 
    locationsController.searchRecent(req, res);
  })

  app.get('/', (req, res) => {
    res.writeHead(200, 'Content-Type', 'application/json'); 
    res.end('Servicio corriendo ok');
  })

  app.listen(port, function () { logger.info({ action: 'Service running on port ' + port}) } )
}

runServer();








